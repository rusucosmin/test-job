FROM liabifano/executor:bd0ce94

COPY setup.py /test-job/
COPY requirements.txt /test-job/
COPY src/ /test-job/src/

RUN find . | grep -E "(__pycache__|\.pyc$)" | xargs rm -rf
RUN pip install -U -r test-job/requirements.txt
RUN pip install test-job/.

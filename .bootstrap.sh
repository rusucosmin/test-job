#!/usr/bin/env bash

set -eo pipefail

DEFAULT_JOB='job-skeleton'
FILES="Dockerfile Makefile setup.py"
FOLDERS="src/${DEFAULT_JOB}/"



while getopts ":j:" opt; do
  case $opt in
      j) JOB_NAME="$OPTARG";;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done



function check-inputs () {

    if [[ -z "$JOB_NAME" ]]; then
        echo "You must specify the job name with the flag -j"
        exit 1
    fi

}


function rename-inside () {

    for f in $FILES
    do
        sed -i.bak "s/${DEFAULT_JOB}/${JOB_NAME}/" $f
        rm -- "${f}.bak"
    done;

}


function undo-rename-inside() {

    for f in $FILES
    do
        sed -i.bak "s/${JOB_NAME}/${DEFAULT_JOB}/" $f
        rm -- "${f}.bak"
    done;

}


function rename-folders () {

    for f in $FOLDERS
    do
        NEWFOLDER=${f//${DEFAULT_JOB}/${JOB_NAME}}
        mv $f ${NEWFOLDER}
    done;

}

function undo-rename-folders() {
    FOLDERS="src/${DEFAULT_JOB}/"

    for f in $FOLDERS
    do
        OLDFOLDER=${f//${DEFAULT_JOB}/${JOB_NAME}}
        NEWFOLDER=${f//${JOB_NAME}/"${DEFAULT_JOB}"}
        mv ${OLDFOLDER} ${NEWFOLDER}
    done;
}



function up () {

    (check-inputs)
    (rename-inside)
    (rename-folders)

}


function down () {

    (check-inputs)
    (undo-rename-inside)
    (undo-rename-folders)

}


case "${@: -1}" in
  (up)
    up
    exit 0
    ;;
  (down)
      down
    exit 0
    ;;
  (*)
    echo "Usage: $0 { up | down }"
    exit 2
    ;;
esac

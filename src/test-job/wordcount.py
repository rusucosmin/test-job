import os

RESOURCES_PATH = os.path.join(os.path.abspath(os.path.join(__file__, '../../..')), 'data')
INPUT_PATH = os.path.join(RESOURCES_PATH, 'data.in')

with open(INPUT_PATH, 'r') as f:
  count = {}
  for line in f.readlines():
    for w in line.split():
      if w not in count:
        count[w] = 0
      count[w] += 1


OUTPUT_PATH = os.path.join(RESOURCES_PATH, 'data.out')
with open(OUTPUT_PATH, 'w') as f:
  for key, value in count.items():
    f.write(key)
    f.write(' ')
    f.write(str(value))
    f.write('\n')
